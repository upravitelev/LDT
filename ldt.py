#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from psychopy import core, gui, visual, event
import pandas as pd
from sklearn.utils import shuffle
from datetime import datetime
from functions import ptdir_f, sysinfo_f, log_f

# experiment's meta
expName = 'LDT_simple'
expInfo = {'participant': '', 'age': '', 'sex': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK is False:
    core.quit()  # user pressed cancel
expInfo['date'] = datetime.now().strftime('%Y_%m_%d')

# create participant's  dir and save sysinfo/log
dirName = ptdir_f(expInfo)
sysinfo_f(dirName)
log_f(dirName)

# setup the window
win = visual.Window(size=[1920, 1080], monitor='laptop', fullscr=True)

# store frame rate of monitor if we can measure it successfully
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] is None:
    frameDur = 1.0/round(expInfo['frameRate'])
else:
    frameDur = 1.0/60.0 # couldn't get a reliable measure so guess

# setup instruction
instr_text = open('stimuli/instruction','r').read()
instr_text = instr_text.decode('utf8')
instruction = visual.TextStim(win, text=instr_text, pos=(0, 0), wrapWidth=1.6, units='norm')

# setup stimuli
fixation = visual.TextStim(win, text="+", pos=(0, 0), height=0.3, color='White')
mask = visual.TextStim(win, text="######", pos=(0, 0), height=0.3, color='White')
response = visual.TextStim(win, text="?", pos=(0, 0), height=0.3, color='White')

# stimuli import
stimuli = pd.read_csv("stimuli/stimuli.csv", encoding='utf8')
seq_s = range(len(stimuli))
seq_s[5:] = shuffle(seq_s[5:])
stimuli['seq_s'] = seq_s

# drawing the instruction
while True:
    instruction.draw()
    win.flip()
    if len(event.getKeys(keyList=['space'])) > 0:
        break

# drawing the fixation
fixation.draw()
win.flip()
core.wait(1)

# create dataframe for responses
tempData = pd.DataFrame(columns=['stimulus', 'key', 'accuracy', 'rt'])

# start the timer
timer = core.Clock()

# drawing stimuli
for i in range(len(stimuli)):
    st = stimuli[stimuli.seq_s == i].stimulus.item()
    stimulus = visual.TextStim(win, text=st, pos=(0, 0), height=0.4, wrapWidth=1.5, units='norm')

    # presenting stimulus
    stimulus.draw()
    win.flip()
    core.wait(0.2)

    # presenting mask
    mask.draw()
    win.flip()
    core.wait(0.1)

    # collecting response
    fTime = timer.getTime()
    while True:
        response.draw()
        win.flip()
        responseKey = event.getKeys(keyList=['z', 'm'])
        if len(responseKey) > 0:
            kTime = timer.getTime()
            tempData = tempData.append({'stimulus': st, 'key': ''.join(responseKey), 'rt': kTime-fTime}, ignore_index=True)
            break
    core.wait(0.5)

# define the accuracy of the responses
res = pd.merge(tempData, stimuli, on='stimulus')
res.ix[(res.key == "m") & (res.s_type == "word"), 'accuracy'] = 1
res.ix[(res.key != "m") & (res.s_type == "word"), 'accuracy'] = 0
res.ix[(res.key == "z") & (res.s_type == "pseudoword"), 'accuracy'] = 1
res.ix[(res.key != "z") & (res.s_type == "pseudoword"), 'accuracy'] = 0

# save result
res.to_csv(dirName + '/result.csv', encoding='utf8')

# feedback statistics
res = res[5:]
correct_words = float(len(res[(res.s_type == 'word') & (res.accuracy == 1)]))
total_words = len(res[res.s_type == 'word'])
rt_median_word = round(res[(res.s_type == 'word') & (res.accuracy == 1)].rt.median() * 1000, 2)

correct_pseudowords = float(len(res[(res.s_type == 'pseudoword') & (res.accuracy == 1)]))
total_pseudowords = len(res[res.s_type == 'pseudoword'])
rt_median_pseudoword = round(res[(res.s_type == 'pseudoword') & (res.accuracy == 1)].rt.median() * 1000, 2)

pt_results = '''Спасибо за участие в эксперименте.
\n
Всего слов ''' + str(total_words) + '''
верно определено ''' + str(float(correct_words)/total_words * 100) + '''% слов
RT median ''' + str(rt_median_word) + ''' ms
\n
Всего псевдослов ''' + str(total_pseudowords) + '''
верно определено ''' + str(float(correct_pseudowords)/total_pseudowords * 100) + '''% слов
RT median ''' + str(rt_median_pseudoword) + ''' ms
\n
Нажмите ПРОБЕЛ для завершения'''

# drawing feedback
pt_results = pt_results.decode('utf8')
endInstr = visual.TextStim(win, text=pt_results, pos=(0, 0), height=0.1, wrapWidth=1.5, units='norm')
event.clearEvents()

while True:
    endInstr.draw()
    win.flip()
    if len(event.getKeys(keyList=['space'])) > 0:
        break

# close the win objects
win.close()
