#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import division
from psychopy import core, gui, visual, event, monitors, data, logging
import pandas as pd
import random
from datetime import datetime
from functions import ptdir_f, sysinfo_f

# experiment's meta
expName = 'LDT_simple'
expInfo = {'participant': '', 'age': '', 'sex': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK is False:
    core.quit()  # user pressed cancel
expInfo['date'] = datetime.now().strftime('%Y_%m_%d')

# create participant's  dir and save sysinfo/log
dirName = ptdir_f(expInfo)
sysinfo_f(dirName)

# Setup filename for saving
filename = dirName + '/%s_%s_%s' %(expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath=None,
    savePickle=True, saveWideText=True,
    dataFileName=filename)

logFile = logging.LogFile(filename+'.log', level=logging.EXP)

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# monitor setup
# xrandr | grep ' connected'
mon = monitors.Monitor(name='asus_laptop')
mon.setSizePix((1920, 1080))
mon.setWidth(34.4)
mon.setDistance(65)
mon.saveMon()

# mon = monitors.Monitor(name='iiyama')
# mon.setSizePix((2560, 1140))
# mon.setWidth(59.7)
# mon.setDistance(65)
# mon.saveMon()

# window setup
win = visual.Window(size=[1920, 1080], monitor=mon, fullscr=False, units='deg', useFBO=False)

# instruction setup
instr_text = open('stimuli/instruction','r').read()
instr_text = instr_text.decode('utf8')
instruction = visual.TextStim(win, text=instr_text, pos=(0, 0), wrapWidth=26)

# tech.symbols setup
fixation = visual.TextStim(win, text="+", pos=(0, 0), height=2, color='White')
mask = visual.TextStim(win, text="######", pos=(0, 0), height=2, color='White')
response = visual.TextStim(win, text="?", pos=(0, 0), height=2, color='White')

# stimuli import
stimuli = pd.read_csv("stimuli/stimuli.csv", encoding='utf8')
seq_s = range(len(stimuli))
seq_s = seq_s[:5] + random.sample(seq_s[5:], len(seq_s[5:]))

# drawing the instruction
instruction.draw()
win.flip()
event.waitKeys(keyList=['space'])

# drawing the fixation
fixation.draw()
win.flip()
core.wait(1)

# create dataframe for responses
tempData = pd.DataFrame(columns=['stimulus', 'key', 'accuracy', 'rt'])

# lists of allowed keys. also cyrillic keys are included
wordKeys = ['m', 'M', u'ь', u'Ь']
pseudowordKeys = ['z', 'Z', u'я', u'Я']
allowedKeys = wordKeys + pseudowordKeys + ['escape']

# start the timer
timer = core.Clock()

# drawing stimuli
for i in seq_s:
    st = stimuli['stimulus'][i]
    stimulus = visual.TextStim(win, text=st, pos=(0, 0), height=2)

    # presenting stimulus
    stimulus.draw()
    win.flip()
    core.wait(0.2)

    # presenting mask
    mask.draw()
    win.flip()
    core.wait(0.05)

    # presenting ? symbol
    response.draw()
    win.flip()

    # collecting response
    fTime = timer.getTime()
    responseKey = event.waitKeys(keyList=allowedKeys, timeStamped=timer)
    key, rTime = responseKey[0]
    rt = rTime - fTime
    tempData = tempData.append({'stimulus': st, 'key': key, 'rt': rt}, ignore_index=True)

    if key == 'escape':
        endExpNow = True
    else:
        thisExp.addData('stimuli', st)
        thisExp.addData('answer', key)
        thisExp.addData('rt', rt)
        thisExp.addData('rTime', rTime)
        thisExp.addData('fTime', fTime)

    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    thisExp.nextEntry()

    core.wait(0.5)

# define the accuracy of the responses
res = pd.merge(tempData, stimuli, on='stimulus')
res.ix[res.key.isin(wordKeys) & (res.s_type == "word"), 'accuracy'] = 1
res.ix[~res.key.isin(wordKeys) & (res.s_type == "word"), 'accuracy'] = 0
res.ix[res.key.isin(pseudowordKeys) & (res.s_type == "pseudoword"), 'accuracy'] = 1
res.ix[~res.key.isin(pseudowordKeys) & (res.s_type == "pseudoword"), 'accuracy'] = 0

# save result
res.to_csv(dirName + '/result.csv', encoding='utf8', index_label='index')

# feedback statistics
res = res[5:]
correct_words = len(res[(res.s_type == 'word') & (res.accuracy == 1)])
total_words = len(res[res.s_type == 'word'])
rt_median_word = res[(res.s_type == 'word') & (res.accuracy == 1)].rt.median()
stat_words = (total_words, correct_words/total_words * 100, rt_median_word * 1000)

correct_pseudowords = len(res[(res.s_type == 'pseudoword') & (res.accuracy == 1)])
total_pseudowords = len(res[res.s_type == 'pseudoword'])
rt_median_pseudoword = res[(res.s_type == 'pseudoword') & (res.accuracy == 1)].rt.median()
stat_pseudowords = (total_pseudowords, correct_pseudowords/total_pseudowords * 100, rt_median_pseudoword * 1000)

pt_results = u'''
Всего слов %d
верно определено %.2f%% слов
RT median %.2f ms
\n
Всего псевдослов %d
верно определено %.2f%% псевдослов
RT median %.2f ms
\n
Нажмите ПРОБЕЛ для завершения''' % (stat_words + stat_pseudowords)

# drawing feedback
endInstr = visual.TextStim(win, text=pt_results, pos=(0, 0), wrapWidth=20)
endInstr.draw()
win.flip()
event.waitKeys(keyList=['space'])
event.clearEvents()

# the end
bye = visual.TextStim(win, text=u'Спасибо за участие!', pos=(0, 0), wrapWidth=20)
bye.draw()
win.flip()
core.wait(1)

# close the win objects
win.close()
core.quit()
