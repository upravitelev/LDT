#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from psychopy import visual, core, event, data, logging, gui, misc, monitors
from datetime import datetime

# задаем монитор и объект окна
monitorName = 'testMonitor'
mon = monitors.Monitor(monitorName, distance=65)
win = visual.Window([400,300],
                    units='deg',
                    fullscr=False,
                    useFBO=False,
                    monitor='testMonitor',
                    color=(1, 1, 1))

# блок по сбору данных об испытуемом
expName = 'LDT_simple'
expInfo = {'participant': '', 'age': '', 'sex': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK is False:
    core.quit()  # user pressed cancel
expInfo['date'] = datetime.now().strftime('%Y_%m_%d')

print(expInfo)

# тестируем ExperimentHandler
info = data.ExperimentHandler(
    name=expInfo,
    version='',
    extraInfo=expInfo,
    runtimeInfo=None,
    originPath=None,
    savePickle=True,
    saveWideText=True,
    dataFileName='info_by_handler',
    autoLog=True)

win.close()