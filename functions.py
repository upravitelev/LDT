def ptdir_f(meta_exp):
    import os
    count = 0
    for root, dirs, files in os.walk('data'):
        count += len(dirs)
    dirName = 'data' + os.path.sep + '%s_%s' % ('pt' + str(count + 1), meta_exp['date'])
    os.makedirs(dirName)
    return dirName


def sysinfo_f(dirname):
    import platform
    sf = open(dirname + '/sysinfo.txt', 'a')
    sf.write('Version: ' + platform.python_version() + "\n")
    sf.write('Compiler: ' + platform.python_compiler() + "\n")
    sf.write('Build: ' + " ".join(platform.python_build()) + "\n")
    sf.write('Normal: ' + platform.platform() + "\n")
    sf.write('Aliased: ' + platform.platform(aliased=True) + "\n")
    sf.write('Terse: ' + platform.platform(terse=True) + "\n")
    sf.write('uname: ' + " ".join(platform.uname()) + "\n")
    sf.write('system: ' + platform.system() + "\n")
    sf.write('node: ' + platform.node() + "\n")
    sf.write('release: ' + platform.release() + "\n")
    sf.write('version: ' + platform.version() + "\n")
    sf.write('machine: ' + platform.machine() + "\n")
    sf.write('processor: ' + platform.processor() + "\n")
    sf.close()


def log_f(dirname):
    from psychopy import logging
    logging.LogFile(dirname + '/log.txt', level=logging.EXP)
    logging.console.setLevel(logging.WARNING)

