#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from psychopy import visual, event

# создаем/рисуем объект окна
win = visual.Window(
    size=[500, 400],
    units='pix',
    fullscr=False
)

# играемся с чтением клавиатуры
# keys = event.getKeys()
# res = 'you pressed %s key, type=%s' % (keys, type(keys))

# играемся с измерением frame rate
frameRate=win.getActualFrameRate()
if frameRate is not None:
    frameDur = 1.0/round(frameRate)
else:
    frameDur = 1.0/60.0 # couldn't get a reliable measure so guess
res = 'frame rate = %f\nframe duration = %f\n\n press "space" for exit' % (frameRate, frameDur)

# текст результата
endInstr = visual.TextStim(win, text=res, pos=(0, 0), height=0.1, wrapWidth=1.5, units='norm')

# отрисоваем последнее окно
while True:
    endInstr.draw()
    win.flip()
    if len(event.getKeys(keyList=['space'])) > 0:
        break

# ставим паузу, чтобы посмотреть окно
# core.wait(5)

# закрываем окно
win.close()